<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190605070745 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, identity VARCHAR(255) NOT NULL, password VARCHAR(128) NOT NULL, api_token VARCHAR(64) DEFAULT NULL, recovery_token VARCHAR(64) DEFAULT NULL, recovery_token_expires_on DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dhikr_group (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, appeal LONGTEXT DEFAULT NULL, endowment LONGTEXT DEFAULT NULL, INDEX IDX_19365338A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dhikr_item (id INT AUTO_INCREMENT NOT NULL, dhikr_group_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, transcript LONGTEXT NOT NULL, arabic LONGTEXT DEFAULT NULL, current INT NOT NULL, target INT NOT NULL, inorder INT NOT NULL, INDEX IDX_58DE429D1215EA0F (dhikr_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dhikr_group ADD CONSTRAINT FK_19365338A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE dhikr_item ADD CONSTRAINT FK_58DE429D1215EA0F FOREIGN KEY (dhikr_group_id) REFERENCES dhikr_group (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dhikr_group DROP FOREIGN KEY FK_19365338A76ED395');
        $this->addSql('ALTER TABLE dhikr_item DROP FOREIGN KEY FK_58DE429D1215EA0F');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE dhikr_group');
        $this->addSql('DROP TABLE dhikr_item');
    }
}
