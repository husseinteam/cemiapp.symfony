<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\DhikrGroup;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/dhikr-group")
 */
class DhikrGroupController extends GenericController
{
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        parent::__construct($em, $translator);
    }
    function class()
    {
        return DhikrGroup::class;
    }
    function resource()
    {
        return 'dhikr-group';
    }
    function fill(&$wireframe, $json)
    {
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['identity' => '25871077074']);
        $wireframe->setAppeal($json['appeal'])
            ->setCurrent($json['current'])
            ->setEndowment($json['endowment'])
            ->setName($json['name'])
            ->setUser($user);
    }
    
}
