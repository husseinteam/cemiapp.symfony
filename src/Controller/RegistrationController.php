<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\TokenAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/auth")
 */
class RegistrationController extends AbstractController
{
    private $em;
    private $passwordEncoder;
    private $translator;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $this->em = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * @Route("/register", name="app_register", methods={"GET","POST"})
     */
    public function register(Request $request, GuardAuthenticatorHandler $guardHandler, TokenAuthenticator $authenticator): JsonResponse
    {
        $credentials = json_decode($request->getContent(), true);

        $existingUser = $this->em->getRepository(User::class)->findOneBy(['identity' => $credentials['identity']]);
        if (null === $existingUser) {
            $user = new User();
            $user->setEmail(in_array('email', $credentials) ? $credentials['email'] : 'no@email.com');
            $user->setIdentity($credentials['identity']);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $credentials['password']
                )
            );
            $this->em->persist($user);
            $this->em->flush();
            // do anything else you need here, like send an email
            $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );

            $data = [
                'message' => $this->translator->trans('user_registered', ['identity' => $credentials['identity']])
            ];
            return new JsonResponse($data, Response::HTTP_OK);
        } else {
            $data = [
                'message' => $this->translator->trans('user_exists', ['identity' => $existingUser->getUsername()])
            ];
            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }
    }
}
