<?php

namespace App\Controller;

use App\Engines\HttpEngine;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Engines\ReflectEngine;

abstract class GenericController extends AbstractController
{

    protected $em;
    private $repo;
    protected $translator;

    abstract protected function class();
    abstract protected function resource();
    abstract protected function fill(&$wireframe, $json);

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->em = $entityManager;
        $this->translator = $translator;
        $this->repo = $this->em->getRepository($this->class());
    }

    protected function serializeData($data): Response
    {
        return HttpEngine::get()->serialize($data);
    }


    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        $data = [
            'all' => $this->repo->findAll(),
        ];
        return $this->serializeData($data);
    }

    /**
     * @Route("/new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {
        $json = (array) $request->request->all();
        $c = $this->class();
        $entity = new $c;
        $this->fill($entity, $json);
        $this->em->persist($entity);
        $this->em->flush();

        $data = [
            'new' => $entity,
            'message' => $this->translator->trans(
                'item_generated',
                ['item' => $this->translator->trans($this->resource())]
            ),
        ];
        return $this->serializeData($data);
    }


    /**
     * @Route("/{id}/edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $id)
    {
        $json = (array) $request->request->all();
        $json['id'] = $id;
        $entity = ReflectEngine::get()->arrayToObject($json, $this->class());
        $this->em->persist($entity);
        $this->em->flush();
        $data = [
            'edit' => $entity,
            'message' => $this->translator->trans(
                'item_updated',
                ['item' => $this->translator->trans($this->resource())]
            ),
        ];
        return $this->serializeData($data);
    }

    /**
     * @Route("/delete/{id}", methods={"DELETE"})
     */
    public function delete($id)
    {
        $entity = $this->em->getRepository($this->class())->findOneById($id);
        $this->em->remove($entity);
        $this->em->flush();
        $data = [
            'delete' => $entity,
            'message' => $this->translator->trans(
                'item_deleted',
                ['item' => $this->translator->trans($this->resource())]
            ),
        ];
        return $this->serializeData($data);
    }
}
