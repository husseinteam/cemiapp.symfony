<?php

namespace App\Controller;

use App\Engines\HttpEngine;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Translation\TranslatorInterface;
use Ramsey\Uuid\Uuid;

/**
 * @Route("/auth")
 */
class LoginController extends AbstractController
{

    private $em;
    private $passwordEncoder;
    private $translator;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $this->em = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
    }

    /**
     * @Route("/tokenize", name="app_tokenize", methods={"POST"})
     */
    public function tokenize(Request $request): Response
    {
        $credentials = (array) $request->request->all();
        $user = $this->em->getRepository(User::class)->findOneBy(['identity' => $credentials['identity']]);

        if ($user) {
            if ($this->passwordEncoder->isPasswordValid($user, $credentials['password'])) {

                if (null === $user->getApiToken()) {
                    $user->setApiToken(Uuid::uuid4()->toString());
                }

                $lastApiTokenGenerationTime = $user->getApiTokenGenerationTime();
                $diff = $lastApiTokenGenerationTime->diff(new \DateTime());
                $minutes = $diff->days * 24 * 60;
                $minutes += $diff->h * 60;
                $minutes += $diff->i;

                if ($minutes > 14) {
                    $user->setApiToken(Uuid::uuid4()->toString());
                }

                $this->em->flush();
                $data = [
                    'message' => $this->translator->trans('user_authenticated', ['identity' => $credentials['identity']]),
                    "success" => true,
                    "token" => $user->getApiToken(),
                ];
                return HttpEngine::get()->serialize($data);
            }
        }
        $data = [
            'message' => $this->translator->trans('identity_not_found', ['identity' => $credentials['identity']]),
            'success' => false
        ];
        return $this->json($data, Response::HTTP_FORBIDDEN);
    }
}
