<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/user")
 */
class UserController extends GenericController
{
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        parent::__construct($em, $translator);
    }
    function class()
    {
        return User::class;
    }
    function resource()
    {
        return 'user';
    }
    function fill(&$wireframe, $json)
    {
        $wireframe->setEmail($json['email'])
            ->setIdentity($json['identity'])
            ->setApiToken($json['apiToken'])
            ->setPassword($json['password']);
    }

    /**
     * @Route("/signedin", name="curuser")
     */
    public function signedin_user(Security $security): Response
    {
        $user = $security->getUser();
        $data = $data = [
            'message' => $this->translator->trans('signedin_user_fetched'),
            "success" => true,
            "user" => $user,
            "token" => $user->getApiToken(),
        ];
        return $this->serializeData($data);
    }
}
