<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Security\Core\Security;
use App\Entity\DhikrGroup;
use App\Entity\DhikrItem;
use App\Entity\User;

class AppFixtures extends Fixture
{
    private $security;
    private $encoder;
    private $dhikrGroupsPath;

    public function __construct(Security $security, UserPasswordEncoderInterface $encoder, $dhikrGroupsPath)
    {
        $this->security = $security;
        $this->encoder = $encoder;
        $this->dhikrGroupsPath = $dhikrGroupsPath;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setIdentity('25871077074');
        $password = $this->encoder->encodePassword($user, '8081');
        $user->setPassword($password);
        $manager->persist($user);
        $manager->flush();

        $finder = new Finder();
        $finder->files()->in($this->dhikrGroupsPath)->name('*.json');
        foreach ($finder as $file) {
            $content = file_get_contents($file->getRealPath());
            $dhikrGroupJson = json_decode($content, true);
            $dhikrGroupEntity = new DhikrGroup();
            $dhikrGroupEntity
                ->setUser($user)
                ->setName($dhikrGroupJson['name'])
                ->setAppeal($dhikrGroupJson['appeal'])
                ->setEndowment($dhikrGroupJson['endowment'])
                ->setCount($dhikrGroupJson['count']);
            $manager->persist($dhikrGroupEntity);
            $manager->flush();
            foreach ($dhikrGroupJson['dhikrItems'] as $dhikrItemJson) {
                $dhikrItemEntity = new DhikrItem();
                $dhikrItemEntity
                    ->setDhikrGroup($dhikrGroupEntity)
                    ->setTitle($dhikrItemJson['title'])
                    ->setTranscript($dhikrItemJson['transcript'])
                    ->setTarget($dhikrItemJson['target'])
                    ->setCount($dhikrItemJson['count'])
                    ->setInorder($dhikrItemJson['inorder']);
                $manager->persist($dhikrItemEntity);
            }
            $manager->flush();
        }
    }
}
