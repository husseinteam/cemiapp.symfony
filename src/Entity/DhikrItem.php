<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DhikrItemRepository")
 */
class DhikrItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $transcript;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $arabic;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\Column(type="integer")
     */
    private $target;

    /**
     * @ORM\Column(type="integer")
     */
    private $inorder;

    /**
     * @ORM\ManyToOne(targetEntity="DhikrGroup", inversedBy="dhikrItems")
     * @JMS\MaxDepth(1)
     */
    private $dhikrGroup;

    public function getDhikrGroup()
    {
        return $this->dhikrGroup;
    }

    public function setDhikrGroup($dhikrGroup): self
    {
        $this->dhikrGroup = $dhikrGroup;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTranscript(): ?string
    {
        return $this->transcript;
    }

    public function setTranscript(string $transcript): self
    {
        $this->transcript = $transcript;

        return $this;
    }

    public function getArabic(): ?string
    {
        return $this->arabic;
    }

    public function setArabic(?string $arabic): self
    {
        $this->arabic = $arabic;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getTarget(): ?int
    {
        return $this->target;
    }

    public function setTarget(int $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getInorder(): ?int
    {
        return $this->inorder;
    }

    public function setInorder(int $inorder): self
    {
        $this->inorder = $inorder;

        return $this;
    }
}
