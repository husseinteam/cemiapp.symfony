<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DhikrGroupRepository")
 */
class DhikrGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $appeal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $endowment;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="dhikrGroups")
     * @JMS\MaxDepth(1)
     */
    private $user;

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @ORM\OneToMany(targetEntity="DhikrItem", mappedBy="dhikrGroup")
     */
    private $dhikrItems;

    public function getDhikrItems()
    {
        return $this->dhikrItems;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAppeal(): ?string
    {
        return $this->appeal;
    }

    public function setAppeal(?string $appeal): self
    {
        $this->appeal = $appeal;

        return $this;
    }

    public function getEndowment(): ?string
    {
        return $this->endowment;
    }

    public function setEndowment(?string $endowment): self
    {
        $this->endowment = $endowment;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
