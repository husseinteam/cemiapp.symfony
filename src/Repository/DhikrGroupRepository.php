<?php

namespace App\Repository;

use App\Entity\DhikrGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DhikrGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DhikrGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DhikrGroup[]    findAll()
 * @method DhikrGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DhikrGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DhikrGroup::class);
    }

    // /**
    //  * @return DhikrGroup[] Returns an array of DhikrGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DhikrGroup
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
