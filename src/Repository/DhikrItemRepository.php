<?php

namespace App\Repository;

use App\Entity\DhikrItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DhikrItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method DhikrItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method DhikrItem[]    findAll()
 * @method DhikrItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DhikrItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DhikrItem::class);
    }

    // /**
    //  * @return DhikrItem[] Returns an array of DhikrItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DhikrItem
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
