<?php

namespace App\Engines;

use Symfony\Component\HttpFoundation\Response;

class HttpEngine
{
    /**
     *
     * @var HttpEngine
     */
    private static $instance;

    public static function get()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private $serializer;

    public function __construct()
    {
        $serializeBuilder = \JMS\Serializer\SerializerBuilder::create();
        $serializeBuilder->setPropertyNamingStrategy(new \JMS\Serializer\Naming\IdenticalPropertyNamingStrategy());
        $this->serializer = $serializeBuilder->build();
    }

    public function serialize($data, $statusCode = Response::HTTP_OK): Response
    {
        $jsonContent = $this->serializer->serialize($data, 'json');
        return new Response($jsonContent, $statusCode);
    }
}
