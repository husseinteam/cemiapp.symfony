<?php

namespace App\Tests;

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

global $kernel;

class GenericWebTest extends WebTestCase
{
    /**
     *
     * @var GenericWebTest
     */
    private static $instance;
    private $_env;

    public function __construct()
    {
        parent::__construct();
        self::bootKernel();
        $dotenv = new Dotenv(true);
        $dotenv->loadEnv(
            $this->service('kernel')->getProjectDir() .
                '/.env.test'
        );
        $this->_env = $_ENV;
    }

    public static function get()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function env(string $key): string
    {
        return $this->_env[$key];
    }

    public function service($keyOrClass)
    {
        return self::$container->get($keyOrClass);
    }
}
