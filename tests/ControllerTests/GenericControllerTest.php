<?php

namespace App\Tests\ControllerTests;

use Symfony\Component\Translation\TranslatorInterface;
use App\Engines\ReflectEngine;
use App\Tests\GenericWebTest;

abstract class GenericControllerTest extends GenericWebTest
{
    abstract protected function resource(): string;
    abstract protected function wireframe(): object;
    abstract protected function editors(): array;
    abstract protected function editProps(): array;
    abstract protected function class();

    private $translator;
    private $em;

    public function __construct()
    {
        parent::__construct();
        $this->translator = $this->service(TranslatorInterface::class);
        $this->em = $this->service('doctrine.orm.entity_manager');
    }

    private function tokenize(array $credentials): string
    {
        $response = $this->clientResponse('POST', 'login', $credentials, true);
        $this->assertEquals(200, $response->getStatusCode());
        $json = json_decode($response->getContent(), true);
        return $json['token'];
    }

    private function clientResponse(string $method, string $path, array $data = [], bool $login = false)
    {
        $server = $this->env('SERVER');
        $resource = $login ? 'auth' : $this->resource();
        $client = self::createClient();
        $client->request(
            $method,
            "{$server}/{$resource}/{$path}",
            $data,
            [],
            $login ? [
                'CONTENT_TYPE' => 'application/json',
            ] : [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X_AUTH_TOKEN' => $this->tokenize([
                    'identity' => '25871077074',
                    'password' => '8081',
                ]),
            ],
        );
        return $client->getResponse();
    }

    private function editWireFrame(): object
    {
        $wireframe = $this->wireframe();
        $editProps = $this->editProps();
        foreach ($this->editors() as $prop => $editor) {
            if ($prop !== 'id') {
                $wireframe = $editor($wireframe, $editProps[$prop]);
            }
        }
        return $wireframe;
    }

    public function testNew()
    {
        $response = $this->clientResponse(
            'POST',
            'new',
            ReflectEngine::get()->objectToArray($this->wireframe())
        );
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertTrue(ReflectEngine::get()
            ->equalsObjectToArray($this->wireframe(), $json['new'], ['id']));
        $this->assertEquals(
            $json['message'],
            $this->translator->trans(
                'item_generated',
                ['item' => $this->translator->trans($this->resource())]
            )
        );
    }

    public function testIndex()
    {
        $response = $this->clientResponse('GET', '');
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertGreaterThan(0, count($json['all']));
    }

    public function testEdit()
    {
        $wireframe = $this->editWireFrame();
        $lastWireframeId = $this->em->getRepository($this->class())
            ->findOneBy([], ['id' => 'desc'])->getId();
        $response = $this->clientResponse(
            'POST',
            "{$lastWireframeId}/edit",
            ReflectEngine::get()->objectToArray($wireframe),
        );
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertTrue(ReflectEngine::get()
            ->equalsObjectToArray($wireframe, $json['edit'], ['id']));
        $this->assertEquals(
            $json['message'],
            $this->translator->trans(
                'item_updated',
                ['item' => $this->translator->trans($this->resource())]
            )
        );
    }

    public function testDelete()
    {
        $lastWireframeId = $this->em->getRepository($this->class())
            ->findOneBy([], ['id' => 'desc'])->getId();
        $response = $this->clientResponse(
            'DELETE',
            "delete/{$lastWireframeId}"
        );
        $this->assertEquals(200, $response->getStatusCode());

        $json = json_decode($response->getContent(), true);
        $this->assertEquals(
            $json['message'],
            $this->translator->trans(
                'item_deleted',
                ['item' => $this->translator->trans($this->resource())]
            )
        );
    }
}
