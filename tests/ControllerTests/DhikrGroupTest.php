<?php

namespace App\Tests\ControllerTests;

use App\Entity\DhikrGroup;
use App\Repository\UserRepository;
use App\Tests\ValueEngine;

class DhikrGroupTest extends GenericControllerTest
{
    protected function resource(): string
    {
        return 'dhikr-group';
    }
    protected function wireframe(): object
    {
        $user = ValueEngine::get()->service(UserRepository::class)
            ->findOneBy(['identity' => '25871077074']);
        return (new DhikrGroup())
            ->setAppeal('Niyet Ettim Allah Rızası için')
            ->setCurrent(0)
            ->setEndowment('Bağışladım')
            ->setName('Deneme Zikir Grubu')
            ->setUser($user);
    }
    protected function editors(): array
    {
        return [
            'current' => function ($wireframe, $prop) {
                return $wireframe->setCurrent($prop);
            }
        ];
    }
    protected function editProps(): array
    {
        return [
            'current' => 19,
        ];
    }
    protected function class()
    {
        return DhikrGroup::class;
    }
}
